# LeagueDemoProject

LeagueDemoProject is a java application to rank teams in a league by the points of the matches.

All data will be located in a text file under input folder located in /LeagueDemoProject/input

The output be located in an output folder located in /LeagueDemoProject/output

## Prerequisites

- Java 11 (https://www.oracle.com/mx/java/technologies/javase/jdk11-archive-downloads.html)
- IntelliJ 2022.1.3 (https://www.jetbrains.com/es-es/idea/download/#section=windows)
- Lombok Plugin (https://projectlombok.org/setup/intellij)
- File -> Settings -> Enable annotation processors

# Build and Run

1. Click Edit Configurations
2. Select Application
3. Select Java 11 SDK
4. Select com.demo.Main as main class
5. Click Apply and Click OK
6. Click Shift + F10 to Run

# Execute Unit Tests

1. Click Edit Configurations
2. Select JUnit
3. Select Java 11 SDK
4. Select LeagueRankingTest class
5. Click Apply and Click OK
6. Click Shift + F10 to Run

### Notes:

If you want to run a specific test you may do so by specifying the methodName in the JUnit configuration