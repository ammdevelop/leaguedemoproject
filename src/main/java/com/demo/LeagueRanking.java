package com.demo;

import com.demo.entity.TeamGoals;
import com.demo.entity.TeamPoints;
import com.demo.util.FileUtil;
import com.demo.util.StringUtil;
import lombok.*;
import lombok.extern.java.Log;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Log
@Builder
public class LeagueRanking {

    private static final Integer POINTS_PER_WIN = 3;
    private static final Integer POINTS_PER_DRAW = 1;
    private static final Integer POINTS_PER_LOSE = 0;
    private static final String TEAM_SEPARATOR = ",";

    /**
     * Gets the rankings from a file at a default folder
     * @return a boolean containing if the operation was successful
     */
    public boolean getRankingFromFile() {
        var file = FileUtil.findFile(StringUtils.EMPTY);
        if(Objects.nonNull(file)) {
            Scanner scanner = null;
            try {
                scanner = new Scanner(file);
                Map<String, Integer> pointsMap = new HashMap<>();
                while (scanner.hasNextLine()) {
                    List<TeamGoals> matchGoals = new ArrayList<>();
                    var line = scanner.nextLine();
                    var teamLines = line.split(TEAM_SEPARATOR);
                    Arrays.stream(teamLines).forEach(teamLine -> {
                        var team = parseTeamLineToTeamGoals(teamLine);
                        matchGoals.add(team);
                    });
                    setPoints(matchGoals, pointsMap);
                }
                return FileUtil.writeOutput(getResults(pointsMap));
            } catch (FileNotFoundException exception) {
                log.severe("::FileNotFoundException::\n" + exception);
                return false;
            } finally {
                if (Objects.nonNull(scanner)) {
                    scanner.close();
                }
            }
        }
        return false;
    }

    /**
     * Gets the rankings from a given input String
     * @param input the input String
     * @return the rankings
     */
    public String getRankingFromInput(String input) {
        if(StringUtils.isNotBlank(input)) {
            Map<String, Integer> pointsMap = new HashMap<>();
            var lines = input.split("\n");
            Arrays.stream(lines).forEach(line -> {
                List<TeamGoals> matchGoals = new ArrayList<>();
                var teamLines = line.split(TEAM_SEPARATOR);
                Arrays.stream(teamLines).forEach(teamLine -> {
                    var team = parseTeamLineToTeamGoals(teamLine);
                    matchGoals.add(team);
                });
                setPoints(matchGoals, pointsMap);
            });
            return getResults(pointsMap);
        }
        return null;
    }

    /**
     * Set the points of a given match into a map
     * @param matchGoals the matchGoals
     * @param pointsMap the pointsMap
     */
    private void setPoints(List<TeamGoals> matchGoals, Map<String, Integer> pointsMap) {
        if(CollectionUtils.isNotEmpty(matchGoals) && matchGoals.size() == 2) {
            var teamGoals1 = matchGoals.get(0);
            var teamGoals2 = matchGoals.get(1);
            TeamGoals winner;
            TeamGoals loser;
            if (teamGoals1.getGoals().equals(teamGoals2.getGoals())) {
                addPoints(pointsMap, teamGoals1.getName(), teamGoals2.getName(), POINTS_PER_DRAW, POINTS_PER_DRAW);
                return;
            } else if (teamGoals1.getGoals() > teamGoals2.getGoals()) {
                winner = teamGoals1;
                loser = teamGoals2;
            } else {
                winner = teamGoals2;
                loser = teamGoals1;
            }

            addPoints(pointsMap, winner.getName(), loser.getName(), POINTS_PER_WIN, POINTS_PER_LOSE);
        }
    }

    /**
     * Set the points of a given match into a map with desired points for the winner/loser or draw match
     * @param pointsMap the pointsMap
     * @param teamName1 the teamName1
     * @param teamName2 the teamName2
     * @param pointsToWinner the points to the Winner
     * @param pointsToLoser the points to the Loser
     */
    private void addPoints(Map<String, Integer> pointsMap,
                                  String teamName1,
                                  String teamName2,
                                  Integer pointsToWinner,
                                  Integer pointsToLoser) {

        var teamPoints1 = pointsMap.get(teamName1);
        var teamPoints2 = pointsMap.get(teamName2);

        if(Objects.isNull(teamPoints1)) {
            pointsMap.put(teamName1, pointsToWinner);
        } else {
            var newPoints = teamPoints1 + pointsToWinner;
            pointsMap.put(teamName1, newPoints);
        }

        if(Objects.isNull(teamPoints2)) {
            pointsMap.put(teamName2, pointsToLoser);
        } else {
            var newPoints = teamPoints2 + pointsToLoser;
            pointsMap.put(teamName2, newPoints);
        }

    }

    /**
     * Parse the given line into team goals
     * @param teamLine the teamLine to parse
     * @return the TeamGoals
     */
    private TeamGoals parseTeamLineToTeamGoals(String teamLine) {
        return TeamGoals.builder()
                .name(StringUtil.getCharactersFromString(teamLine))
                .goals(StringUtil.getDigitsFromString(teamLine))
                .build();
    }

    /**
     * Build the results from the pointsMap
     * @param pointsMap  the pointsMap
     * @return the ranking results
     */
    private String getResults(Map<String, Integer> pointsMap) {
        List<TeamPoints> teamPoints = sortByKeyAndValue(pointsMap);
        var result = getResults(teamPoints);
        log.info("::Results::\n" + result);
        return result;
    }

    /**
     * Builds the ranking results
     * @param teamPoints the teamPoints to build
     * @return the ranking results
     */
    private String getResults(List<TeamPoints> teamPoints){
        if(CollectionUtils.isNotEmpty(teamPoints)) {
            var stringBuilder = new StringBuilder();
            var place = new AtomicInteger(1);
            var overAllPlace = new AtomicInteger(1);
            var lastTeamPoints = new AtomicInteger(0);

            StringJoiner stringJoiner = new StringJoiner("\n");
            teamPoints.forEach(teamPoint -> {
                var points = teamPoint.getPoints();
                var teamName = teamPoint.getName();
                if (lastTeamPoints.get() > points) {
                    place.set(overAllPlace.get());
                }
                overAllPlace.getAndIncrement();
                lastTeamPoints.set(points);
                stringBuilder.append(place);
                stringBuilder.append(". ");
                stringBuilder.append(teamName);
                stringBuilder.append(", ");
                stringBuilder.append(points);
                stringBuilder.append(" pt");
                if (points != 1) {
                    stringBuilder.append("s");
                }
                stringJoiner.add(stringBuilder.toString());
                stringBuilder.setLength(0);
                stringBuilder.trimToSize();
            });

            return stringJoiner.toString();
        }
        return null;
    }

    /**
     * Converts the map to a sorted list comparing the points and team names
     * @param pointsMap the pointsMap
     * @return a list with sorted values
     */
    private List<TeamPoints> sortByKeyAndValue(Map<String, Integer> pointsMap) {

        return pointsMap.entrySet()
                .stream()
                .map(team -> TeamPoints.builder()
                        .name(team.getKey())
                        .points(team.getValue())
                        .build())
                .collect(Collectors.toList())
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(TeamPoints::getPoints))
                .thenComparing(TeamPoints::getName))
                .collect(Collectors.toList());

    }

}
