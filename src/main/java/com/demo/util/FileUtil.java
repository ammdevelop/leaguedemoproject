package com.demo.util;

import lombok.extern.java.Log;

import java.io.*;
import java.util.Objects;

@Log
public class FileUtil {

    private static final String INPUT_DIRECTORY = "input";
    private static final String OUTPUT_DIRECTORY = "output/";
    private static final String OUTPUT_FILE_NAME = "output";
    private static final String OUTPUT_FILE_EXTENSION = ".txt";

    /**
     * Find a single file from the given path
     * @param directory the directory path
     * @return the file found
     */
    public static File findFile(String directory) {
        String inputDirectory = directory.isBlank() ? INPUT_DIRECTORY : directory;
        var file = new File(inputDirectory);
        log.info("Searching files in: " + file.getAbsoluteFile());
        var files = file.listFiles();
        if(Objects.isNull(files) || files.length <= 0){
            log.severe("Couldn't find any file in " + INPUT_DIRECTORY + " folder");
            return null;
        } else if(files.length == 1){
            return files[0];
        } else {
            log.severe("Only 1 file is allowed to be in " + INPUT_DIRECTORY + " folder");
            return null;
        }
    }

    /**
     * Writes the output to a file in the default directory
     * @param output the output
     * @return a boolean if the operation was successful
     */
    public static boolean writeOutput(String output) {
        return writeOutput(output, OUTPUT_DIRECTORY);
    }

    /**
     * Writes the output to a file in the given directory
     * @param output the output
     * @param directory the directory path
     * @return  a boolean if the operation was successful
     */
    public static boolean writeOutput(String output, String directory) {
        Writer writer;
        try {
            var stringBuilder = new StringBuilder();
            stringBuilder.append(directory);
            stringBuilder.append(OUTPUT_FILE_NAME);
            stringBuilder.append(OUTPUT_FILE_EXTENSION);
            var folder = new File(directory);
            if(!folder.exists()){
                folder.mkdir();
            }
            writer = new BufferedWriter(new FileWriter(stringBuilder.toString()));
            writer.write(output);
            writer.close();
        } catch (IOException exception){
            log.severe("::IOException::\n" + exception);
            return false;
        }
        return true;
    }

}
