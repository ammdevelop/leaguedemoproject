package com.demo.util;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log
public class StringUtil {

    private static final String CHARACTER_REGEX = "\\b[a-zA-Z ]+\\b[a-zA-Z]*";
    private static final String DIGIT_REGEX = "\\d+";
    private static Pattern pattern;

    /**
     * Returns the Characters from the input String
     * @param input the input String
     * @return the parsed String
     */
    public static String getCharactersFromString(String input){
        pattern = Pattern.compile(CHARACTER_REGEX);
        Matcher matcher = pattern.matcher(input);
        if(matcher.find()) {
            return matcher.group(0).trim();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Returns the digits from the input String
     * @param input the input String
     * @return the parsed String as an Integer
     */
    public static Integer getDigitsFromString(String input){
        pattern = Pattern.compile(DIGIT_REGEX);
        Matcher matcher = pattern.matcher(input);
        if(matcher.find()) {
            return Integer.parseInt(matcher.group(0));
        }
        return 0;
    }
}
