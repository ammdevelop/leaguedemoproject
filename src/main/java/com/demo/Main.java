package com.demo;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

@Log
public class Main {

    public static void main(String[] args) {

        var leagueRanking = LeagueRanking.builder().build();
        var successful = leagueRanking.getRankingFromFile();

        if(successful) {
            log.info("File written successfully");
        } else {
            log.severe("There was an error writing the output file");
        }

        System.exit(0);
    }

}

