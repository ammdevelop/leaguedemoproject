package com.demo.entity;

import com.demo.util.StringUtil;
import lombok.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TeamGoals {

    private String name;
    private Integer goals;

}
