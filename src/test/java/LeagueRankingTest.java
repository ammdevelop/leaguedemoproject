import com.demo.LeagueRanking;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class LeagueRankingTest {

    private final String sampleInput =  "Lions 3, Snakes 3\n" +
                                        "Tarantulas 1, FC Awesome 0\n" +
                                        "Lions 1, FC Awesome 1\n" +
                                        "Tarantulas 3, Snakes 1\n" +
                                        "Lions 4, Grouches 0";

    private final String sampleInputTwo =   "Lions 3, Snakes 3\n" +
                                            "Lions 1, FC Awesome 1\n" +
                                            "Lions 1, Tarantulas 0\n" +
                                            "Snakes 3, FC Awesome 3\n" +
                                            "Snakes 1, Tarantulas 1\n" +
                                            "FC Awesome 1, Tarantulas 2";

    private final String sampleOutput =  "1. Tarantulas, 6 pts\n" +
                                         "2. Lions, 5 pts\n" +
                                         "3. FC Awesome, 1 pt\n" +
                                         "3. Snakes, 1 pt\n" +
                                         "5. Grouches, 0 pts";


    private final String sampleOutputTwo =  "1. Lions, 5 pts\n" +
                                            "2. Tarantulas, 4 pts\n" +
                                            "3. Snakes, 3 pts\n" +
                                            "4. FC Awesome, 2 pts";

    private final String notMatchingInput =  "Liokes 3\n" +
                                             "Tarantwesome 0\n" +
                                             "Lioe 1\n" +
                                             "Tarantukes 1\n" +
                                             "Liones 0";


    @Test
    public void testMatchingInputExpectedOutput() {
        var leagueRanking = createLeagueRanking();
        var output = leagueRanking.getRankingFromInput(sampleInput);
        Assert.assertNotNull(output);
        Assert.assertEquals(sampleOutput, output);
    }

    @Test
    public void testMatchingInputTwoExpectedOutputTwo() {
        var leagueRanking = createLeagueRanking();
        var output = leagueRanking.getRankingFromInput(sampleInputTwo);
        Assert.assertNotNull(output);
        Assert.assertEquals(sampleOutputTwo, output);
    }

    @Test
    public void testInputDontMatchAssertNullOutput() {
        var leagueRanking = createLeagueRanking();
        var output = leagueRanking.getRankingFromInput(notMatchingInput);
        Assert.assertNull(output);
    }

    @Test
    public void testEmptyInputAssertNullOutput() {
        var leagueRanking = createLeagueRanking();
        var output = leagueRanking.getRankingFromInput(StringUtils.EMPTY);
        Assert.assertNull(output);
    }

    private LeagueRanking createLeagueRanking() {
        return LeagueRanking.builder().build();
    }
}
